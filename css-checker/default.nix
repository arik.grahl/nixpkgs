{ lib, buildGo118Module, fetchFromGitHub }:

with lib;

buildGo118Module rec {
  pname = "css-checker";
  version = "0.4.1";

  src = fetchFromGitHub {
    owner = "ruilisi";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-lD2uF8zhJG8pVepqxyKKj4GZNB883uDV/9dCMFYJbRs=";
  };

  vendorSha256 = "sha256-4ZCma8Q7FXAWdA1m2M1ltm360Fu65JhELyfIbJBP14M=";
}
