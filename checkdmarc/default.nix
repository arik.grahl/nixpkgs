{ lib, python3 }:

with lib;

python3.pkgs.buildPythonApplication rec {
  pname = "checkdmarc";
  version = "4.4.1";

  src = python3.pkgs.fetchPypi {
    inherit pname version;
    sha256 = "198j09ni9s9pymaarhcgp9r1n30zipa7rrwx8148vspz9p1shaz9";
  };

  pyleri = python3.pkgs.buildPythonApplication rec {
    pname = "pyleri";
    version = "1.3.4";

    src = python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "0khjibscgc8fwkgx0cc7bfxcihxwhmmci8kyjm9ykzpwary0ysng";
    };
  };

  propagatedBuildInputs = with python3.pkgs; [
    dns
    expiringdict
    publicsuffix2
    pyleri
    requests
    timeout-decorator
  ];
}
