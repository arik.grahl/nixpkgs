{ lib, buildGo118Module, fetchFromGitHub }:

with lib;

buildGo118Module rec {
  pname = "heketi";
  version = "10.4.1";

  src = fetchFromGitHub {
    owner = pname;
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-9XwML6j8Yd48CaPGcnNfTomVSovmsEUEBgc3StUasJ0=";
  };

  vendorSha256 = "sha256-TR01Al+/AhAp7tMgnaLQ22ksONbj2+c5GQ1Zp1ikVIU=";

  doCheck = false;

  postInstall = ''
    mv -v $out/bin/go $out/bin/heketi-cli
  '';
}
